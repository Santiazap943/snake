package snake.ui;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import snake.logica.Campo;
import snake.controlador.ControlTouch;

public class MainActivity extends AppCompatActivity {
    private Campo campo;
    private DibujoMatriz dibujoMatriz;
    private ControlTouch controlTouch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.controlTouch = new ControlTouch(this);
        this.dibujoMatriz = new DibujoMatriz(this);
        this.campo = new Campo();
        this.campo.addObserver(this.dibujoMatriz);
        this.campo.iniciarJuego();
    }
    public void moverFicha(String movimiento) {
        this.campo.moverFicha(movimiento);
    }
}