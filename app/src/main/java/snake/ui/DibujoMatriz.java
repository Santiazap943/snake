package snake.ui;

import android.graphics.Color;
import android.graphics.Point;
import android.view.Display;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import snake.logica.Campo;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class DibujoMatriz implements Observer {
    private GridLayout gridLayoutMatriz;
    private TextView textViewMatriz[][];
    private LinearLayout linearLayoutCampo;
    private MainActivity mainActivity;

    public DibujoMatriz(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        this.textViewMatriz = new TextView[Campo.FILAS][Campo.COLUMNAS];
        this.gridLayoutMatriz = new GridLayout(this.mainActivity);
        this.gridLayoutMatriz.setColumnCount(10);
        this.gridLayoutMatriz.setRowCount(20);
        this.linearLayoutCampo = this.mainActivity.findViewById(R.id.linearLayoutCampo);
        Display display = this.mainActivity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        for(int i=0; i<Campo.FILAS; i++){
            for(int j=0; j<Campo.COLUMNAS; j++){
                this.textViewMatriz[i][j] = new TextView(this.mainActivity);
                this.textViewMatriz[i][j].setWidth((int)(width*0.7/10));
                this.textViewMatriz[i][j].setHeight((int)(width*0.7/10));
                this.textViewMatriz[i][j].setBackgroundColor(Color.BLACK);
                this.gridLayoutMatriz.addView(this.textViewMatriz[i][j]);
            }
        }
        this.linearLayoutCampo.addView(this.gridLayoutMatriz);
    }

    @Override
    public void update(Observable o, Object arg) {
        ArrayList<Object> elemento = (ArrayList<Object>) arg;
        int coordenadas[] = (int[])elemento.get(0);
        int color = (int)elemento.get(1);

            this.textViewMatriz[coordenadas[0]][coordenadas[1]].setBackgroundColor(color);
    }
}
