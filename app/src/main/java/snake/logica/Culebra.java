package snake.logica;

public class Culebra{
    private int corY;
    private int corX;

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    private int color;
    public Culebra(int iCorX, int iCorY){
        this.corX = iCorX;
        this.corY = iCorY;
    }

    public int getCorY() {
        return corY;
    }

    public void setCorY(int corY) {
        this.corY = corY;
    }

    public int getCorX() {
        return corX;
    }

    public void setCorX(int corX) {
        this.corX = corX;
    }

    public boolean izquierda(){
            if(this.corX == 0){
                return false;
            }
            int newCulexbra[] = {this.corX--,this.corY};
        return newCulebra;
    }
/*
    public boolean derecha(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][1] == Tetris.COLUMNAS-1){
                return false;
            }
        }
        for(int i=0; i<4; i++){
            this.segmentos[i][1]++;
        }
        return true;
    }

    public boolean abajo(){
        for(int i=0; i<this.segmentos.length; i++){
            if(this.segmentos[i][0] == Tetris.FILAS-1){
                return false;
            }
        }
        for(int i=0; i<4; i++){
            this.segmentos[i][0]++;
        }
        return true;
    }*/
}