package snake.logica;

public class Manzana {
    int corX;
    int corY;
    public Manzana(int iCorX,int iCorY){
        this.corX = iCorX;
        this.corY = iCorY;
    }
    public int getCorX() {
        return corX;
    }

    public void setCorX(int corX) {
        this.corX = corX;
    }

    public int getCorY() {
        return corY;
    }

    public void setCorY(int corY) {
        this.corY = corY;
    }
}
