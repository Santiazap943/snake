package snake.logica;
import android.graphics.Color;
import java.util.ArrayList;
import java.util.Observable;
import snake.util.Constantes;

import static android.text.TextUtils.isEmpty;

public class Campo extends Observable {
    public final static int FILAS = 20;
    public final static int COLUMNAS = 10;
    int campoLogic[][] = new int[20][10];
    private ArrayList <Culebra> culebrita = new ArrayList<Culebra>();
    public void iniciarJuego(){
        this.agregarSegmento();
        this.agregarManzana();
        ArrayList<Object> elemento = new ArrayList<Object>();
        int cor[] = {5, 5};
        elemento.add(cor);
        elemento.add(Color.RED);
        this.setChanged();
        this.notifyObservers(elemento);
    }
    public void agregarSegmento(){
               Culebra newCabeza = new Culebra(5,5);
               culebrita.add(newCabeza);
    }
    public  void agregarManzana(){
        int corX = (int)(Math.random() * 20);
        int corY = (int)(Math.random() * 10);
        /*for(Culebra c:culebra) {
            if(c.getCorX() == corX || c.getCorY() == corY){
                agregarManzana();
            }
        }
        Manzana manzana = new Manzana(corX,corY);*/
    }
    public void moverFicha(String movimiento) {







        int segmentosAnteriores[] = new int[2];
        segmentosAnteriores[0] = this.culebrita.get(culebrita.size() - 1).getCorX();
        ArrayList<Object> elementob = new ArrayList<Object>();
        elementob.add(segmentosAnteriores);
        elementob.add(Color.BLACK);
        this.setChanged();
        this.notifyObservers(elementob);
        int cord[] ={0,0};
        boolean hecho = false;
        if (movimiento == Constantes.IZQUIERDA) {
            cord = this.culebrita.get(0).izquierda();
            if(cord != null){
                hecho = true;
            }
        }/*else if(movimiento == Constantes.DERECHA){
            hecho = this.culebrita.get(culebrita.size()-1).derecha();
        }else if(movimiento == Constantes.ABAJO){
            hecho = this.culebrita.get(culebrita.size()-1).abajo();
        }*/
        if (hecho) {
            Culebra newCul = new Culebra(cord[0], cord[1]);
            culebrita.add(0,newCul);
            ArrayList<Object> elementoc = new ArrayList<Object>();
            int cor[] = {cord[0], cord[1]};
            elementoc.add(cor);
            elementoc.add(Color.RED);
            this.setChanged();
            this.notifyObservers(elementoc);
        }
    }

}